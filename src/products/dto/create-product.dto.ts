import { IsNotEmpty, MinLength, IsInt, Min } from 'class-validator';
export class CreateProductDto {
  @MinLength(4)
  @IsNotEmpty()
  name: string;

  @IsInt()
  @Min(0)
  @IsNotEmpty()
  price: number;
}
